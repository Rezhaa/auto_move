package com.example.payroll.Utils;

import java.security.Principal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.ModelMap;


public class DisplayPageUser {
public static Logger logger = LogManager.getLogger(DisplayPageUser.class.getName());
	
	public DisplayPageUser(Pageable page, Principal principal,ModelMap data){
		data.addAttribute("hal", page.getPageNumber());
		data.addAttribute("jmlhal", page.getPageSize());
	}
}
