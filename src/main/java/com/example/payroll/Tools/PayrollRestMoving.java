package com.example.payroll.Tools;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.payroll.Config.DataSource1.Dao.Jdbc_Payroll_Sysparam_Dao;
import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_Sysparam_Model;
import com.example.payroll.Job.AutoMoveFile;
import com.example.payroll.Utils.Constans;

@RestController
//@Component
public class PayrollRestMoving {
	public static Logger logger = LogManager.getLogger(PayrollRestMoving.class.getName());

	@Autowired
	Method method;
	
	@Autowired
	Jdbc_Payroll_Sysparam_Dao sysparamDao;

	@RequestMapping(value = "/payroll/move/manual", method = RequestMethod.GET)
	public List<Integer> MovingFile() {
		Jdbc_Payroll_Sysparam_Model getIncoming = sysparamDao.getByID(Constans.INCOMING_PATH_ID);
		Jdbc_Payroll_Sysparam_Model getOutgoing = sysparamDao.getByID(Constans.OUTGOINGPATH_ID);
		List<Integer> move = method.moveFiles(getIncoming.getNAME(), getOutgoing.getNAME());
		logger.info("banyak file = " + move.size());
		return move;
	}
}
