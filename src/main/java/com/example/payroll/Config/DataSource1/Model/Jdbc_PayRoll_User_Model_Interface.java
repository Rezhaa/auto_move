package com.example.payroll.Config.DataSource1.Model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface Jdbc_PayRoll_User_Model_Interface {

	Page<Jdbc_Payroll_User_Model> list(String name, Pageable page);

}
