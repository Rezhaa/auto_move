package com.example.payroll.Config.DataSource1;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
public class ScurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	DataSource dataSource;
	
	@Override
	public void configure(HttpSecurity httpSecurity) throws Exception{
		
		httpSecurity.authorizeRequests()
		.antMatchers("/admin/**").hasAnyAuthority("ADMIN")
		.and()
	.formLogin()
		.loginPage("/login")
		.and()
	.logout()
		.logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
	}
	
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception{
		auth.jdbcAuthentication().dataSource(dataSource)
		.passwordEncoder(new BCryptPasswordEncoder())
		.usersByUsernameQuery("select USERNAME, PASSWORD, ENABLED from PAYROLL_USERS where USERNAME =?")
		.authoritiesByUsernameQuery("select USERNAME, ROLE from PAYROLL_AUTHORITIES  where USERNAME =?");
	}
	
	@Autowired
	public void convigureGlobal (AuthenticationManagerBuilder auth) throws Exception{
		auth.inMemoryAuthentication()
		.withUser("admin1").password(bCryptPasswordEncoder.encode("admin")).authorities("ADMIN");
	}
}
