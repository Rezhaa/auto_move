package com.example.payroll.Config.DataSource1.Rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_Sysparam_Model;

public class Jdbc_Payroll_Sysparam_Rowmapper implements RowMapper<Jdbc_Payroll_Sysparam_Model> {

	@Override
	public Jdbc_Payroll_Sysparam_Model mapRow(ResultSet rs, int rowNum) throws SQLException {
		Jdbc_Payroll_Sysparam_Model detail = new Jdbc_Payroll_Sysparam_Model();

		detail.setID(rs.getString("ID"));
		detail.setNAME(rs.getString("NAME"));
		return detail;
	}

}
