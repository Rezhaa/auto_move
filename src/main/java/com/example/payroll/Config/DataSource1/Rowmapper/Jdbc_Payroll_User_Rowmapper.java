package com.example.payroll.Config.DataSource1.Rowmapper;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_User_Model;


public class Jdbc_Payroll_User_Rowmapper implements RowMapper<Jdbc_Payroll_User_Model>{

	@Override
	public Jdbc_Payroll_User_Model mapRow(ResultSet rs, int rowNum) throws SQLException {
		Jdbc_Payroll_User_Model detail = new Jdbc_Payroll_User_Model();
		
		detail.setUSERNAME(rs.getString("USERNAME"));
		detail.setPASSWORD(rs.getString("PASSWORD"));
		detail.setNAME(rs.getString("NAME"));
		detail.setDATE_CREATED(rs.getDate("DATE_CREATED"));
		detail.setUSER_CREATED(rs.getString("USER_CREATED"));
		detail.setDATE_EDITED(rs.getDate("DATE_EDITED"));
		detail.setUSER_EDITED(rs.getString("USER_EDITED"));
		detail.setISDELETE(rs.getString("ISDELETE"));
		detail.setENABLED(rs.getString("ENABLED"));
		return detail;
	}
	


}
