package com.example.payroll.Config.DataSource1;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactory1", 
        transactionManagerRef = "transactionManager1",
        basePackages = {"com.example.payroll.Config.DataSource1"}
        )
public class DtSource1 {
@Autowired Environment environment;
	
	@Primary
    @Bean(name = "dataSource1")
    @ConfigurationProperties(prefix="payroll.datasource.oracle1")
    public DataSource DataSource1() {
        return DataSourceBuilder.create().build();
    }
	
	@Primary
    @Bean(name = "entityManagerFactory1")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory1(
            EntityManagerFactoryBuilder builder,
            @Qualifier("dataSource1") DataSource dataSource1) {
        return builder
                .dataSource(dataSource1)
                .packages("com.example.payroll")
                .persistenceUnit("*")
                .build();
    }
	
	@Primary
    @Bean(name = "transactionManager1")
    public PlatformTransactionManager transactionManager1(
            @Qualifier("entityManagerFactory1") EntityManagerFactory entityManagerFactory1) {
        return new JpaTransactionManager(entityManagerFactory1);
    }
	
	@Autowired
	@Bean(name = "jdbcTemplate1")
    public JdbcTemplate JdbcTemplate1(@Qualifier("dataSource1")DataSource datasource1) {
		JdbcTemplate jdbcTemplateTemp = new JdbcTemplate(datasource1);
		return jdbcTemplateTemp;
    }
}
