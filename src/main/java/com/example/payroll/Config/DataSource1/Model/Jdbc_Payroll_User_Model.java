package com.example.payroll.Config.DataSource1.Model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class Jdbc_Payroll_User_Model {

	@NotNull @NotEmpty(message = "username is mandatory")
	private String USERNAME;
	@NotNull @NotEmpty(message = "password is mandatory")
	private String PASSWORD;
	@NotNull @NotEmpty(message = "name is mandatory")
	private String NAME;
	private String ENABLED;
	private Date DATE_CREATED;
	private String USER_CREATED;
	private String ISDELETE;
	private Date DATE_EDITED;
	private String USER_EDITED;
	private List<String> ROLE_USER;
	private List<Jdbc_Role_Author_Model> ROLE_USER_MODEL;
	
	public String getUSERNAME() {
		return USERNAME;
	}
	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}
	public String getPASSWORD() {
		return PASSWORD;
	}
	public void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	
	
	public String getENABLED() {
		return ENABLED;
	}
	public void setENABLED(String eNABLED) {
		ENABLED = eNABLED;
	}
	public Date getDATE_CREATED() {
		return DATE_CREATED;
	}
	public void setDATE_CREATED(Date dATE_CREATED) {
		DATE_CREATED = dATE_CREATED;
	}
	public String getUSER_CREATED() {
		return USER_CREATED;
	}
	public void setUSER_CREATED(String uSER_CREATED) {
		USER_CREATED = uSER_CREATED;
	}
	
	public String getISDELETE() {
		return ISDELETE;
	}
	public void setISDELETE(String iSDELETE) {
		ISDELETE = iSDELETE;
	}
	public Date getDATE_EDITED() {
		return DATE_EDITED;
	}
	public void setDATE_EDITED(Date dATE_EDITED) {
		DATE_EDITED = dATE_EDITED;
	}
	public String getUSER_EDITED() {
		return USER_EDITED;
	}
	public void setUSER_EDITED(String uSER_EDITED) {
		USER_EDITED = uSER_EDITED;
	}
	public List<String> getROLE_USER() {
		return ROLE_USER;
	}
	public void setROLE_USER(List<String> rOLE_USER) {
		ROLE_USER = rOLE_USER;
	}
	public List<Jdbc_Role_Author_Model> getROLE_USER_MODEL() {
		return ROLE_USER_MODEL;
	}
	public void setROLE_USER_MODEL(List<Jdbc_Role_Author_Model> rOLE_USER_MODEL) {
		ROLE_USER_MODEL = rOLE_USER_MODEL;
	}
	
	
	
}
