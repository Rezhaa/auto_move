package com.example.payroll.Config.DataSource1.Dao;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.payroll.Config.DataSource1.Model.Jdbc_List_Role_Model;
import com.example.payroll.Config.DataSource1.Model.Jdbc_PayRoll_User_Model_Interface;
import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_User_Model;
import com.example.payroll.Config.DataSource1.Model.Jdbc_Role_Author_Model;
import com.example.payroll.Config.DataSource1.Model.Jdbc_Role_User_Model;
import com.example.payroll.Config.DataSource1.Rowmapper.Jdbc_List_Role_Rowmapper;
import com.example.payroll.Config.DataSource1.Rowmapper.Jdbc_Payroll_User_Rowmapper;
import com.example.payroll.Config.DataSource1.Rowmapper.Jdbc_Role_Author_Rowmapper;

@Repository
public class Jdbc_Payroll_User_Dao implements Jdbc_PayRoll_User_Model_Interface{

	@Autowired 
	@Qualifier("jdbcTemplate1")
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("entityManagerFactory1")
	private EntityManagerFactory entityManagerFactory;
	
	
	@Override
	public Page<Jdbc_Payroll_User_Model> list(String name, Pageable page){
		
		String count ="select count(USERNAME) as jml from PAYROLL_USERS where ISDELETE = '0' AND USERNAME like ?";
		int total = jdbcTemplate.queryForObject(count,
                new Object[]{name}, (rs, rowNum) -> rs.getInt("jml")
        );
		
		String query = "Select USERNAME, PASSWORD,NAME, DATE_CREATED, USER_CREATED,DATE_EDITED, USER_EDITED, ISDELETE, ENABLED "
				+ " from ( select  USERNAME, PASSWORD,NAME, DATE_CREATED, USER_CREATED,DATE_EDITED, USER_EDITED, ISDELETE, ENABLED, ROW_NUMBER() OVER (ORDER BY DATE_CREATED) rowRank from PAYROLL_USERS " + 
				"WHERE  USERNAME like ?) where  ISDELETE = 0 ";
		List<Jdbc_Payroll_User_Model> data =  jdbcTemplate.query(query, new Object[] {name}, new Jdbc_Payroll_User_Rowmapper());
	
		return new PageImpl<>(data, page, total);
	}
	
	public Jdbc_Payroll_User_Model InsertClient(Jdbc_Payroll_User_Model model) {
		String query = "insert into PAYROLL_USERS(USERNAME, PASSWORD, NAME, DATE_CREATED,"
				+ "USER_CREATED, ISDELETE, ENABLED) values(?,?,?,?,?,?,?)";
	 jdbcTemplate.update(query, new Object[] {model.getUSERNAME(), model.getPASSWORD()
			,model.getNAME(), model.getDATE_CREATED(),model.getUSER_CREATED(), model.getISDELETE(), model.getENABLED()});
	return model;
	}
	public Jdbc_Payroll_User_Model editClient(Jdbc_Payroll_User_Model model) {
		String query = "update PAYROLL_USERS set NAME=?, PASSWORD=?, DATE_EDITED=?,"
				+ "USER_EDITED=?, ENABLED=? where USERNAME=?";
		jdbcTemplate.update(query, new Object[] {model.getNAME(),model.getPASSWORD(), model.getDATE_EDITED()
				,model.getUSER_EDITED(), model.getENABLED(), model.getUSERNAME()});
		return model;
	}
	
	public Jdbc_Payroll_User_Model detailClient(String username) {
		String query = "select * from PAYROLL_USERS where USERNAME like ?";
		List<Jdbc_Payroll_User_Model> data = jdbcTemplate.query(query, new Object [] {username}, new Jdbc_Payroll_User_Rowmapper());
		 System.out.println(data);
		return data.get(0);
	}
	
	public List<Jdbc_List_Role_Model> listRole(){
		String query = "select * from PAYROLL_ROLE";
		List<Jdbc_List_Role_Model> data = jdbcTemplate.query(query, new Jdbc_List_Role_Rowmapper());
		return data;
	}
	
	
	public Jdbc_Role_User_Model addRole(Jdbc_Role_User_Model roleModel) {
		String query = "insert into PAYROLL_AUTHORITIES(ID, USERNAME, ROLE, DATE_CREATED, USER_CREATED) "
				+ "values(?,?,?,?,?)";
			jdbcTemplate.update(query,new Object[] {roleModel.getID(),roleModel.getUSERNAME(),
					roleModel.getROLE(),roleModel.getDATE_CREATED(),roleModel.getUSER_CREATED()});
		return roleModel;
	}
	public Integer DeleteUserRole(String username) {
		String query = "delete from PAYROLL_AUTHORITIES where USERNAME=? ";
		jdbcTemplate.update(query,new Object[] {username});
		return 1;
	}
	
	public Integer delete(Jdbc_Payroll_User_Model jdbcUserModel) {
		String query = "update PAYROLL_USERS set ISDELETE=?,DATE_EDITED=?,USER_EDITED=? where USERNAME =?";
		jdbcTemplate.update(query,new Object[] {jdbcUserModel.getISDELETE(),jdbcUserModel.getDATE_EDITED(),
				jdbcUserModel.getUSER_EDITED(),jdbcUserModel.getUSERNAME()});
		return 1;
	}
	
	public Integer changePassword(String password, String username) {
		String query = "update PAYROLL_USERS set PASSWORD =? where USERNAME=?";
		jdbcTemplate.update(query, new Object [] {password, username});
		return 1;
	}
	public List<Jdbc_Role_Author_Model> findUserRole(String username){
		String query="select r.role AS ROLE, a.username AS USERNAME, a.role AS ROLEUSER from payroll_role r left join payroll_authorities a on r.role = a.role where username =?";
		return jdbcTemplate.query(query, new Object[] {username}, new Jdbc_Role_Author_Rowmapper());
		
	}
	
}
