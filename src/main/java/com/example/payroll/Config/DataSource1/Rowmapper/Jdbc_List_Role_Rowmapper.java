package com.example.payroll.Config.DataSource1.Rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.payroll.Config.DataSource1.Model.Jdbc_List_Role_Model;


public class Jdbc_List_Role_Rowmapper implements RowMapper<Jdbc_List_Role_Model>{

	@Override
	public Jdbc_List_Role_Model mapRow(ResultSet rs, int rowNum) throws SQLException {
		Jdbc_List_Role_Model detail = new Jdbc_List_Role_Model();
		
		detail.setID(rs.getString("ID"));
		detail.setROLE(rs.getString("ROLE"));
		return detail;
	}

}
