package com.example.payroll.Config.DataSource1.Dao;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_History_Model;
import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_Sysparam_Model;
import com.example.payroll.Config.DataSource1.Rowmapper.Jdbc_Payroll_History_Rowmapper;
import com.example.payroll.Config.DataSource1.Rowmapper.Jdbc_Payroll_Sysparam_Rowmapper;

@Repository
public class Jdbc_Payroll_Sysparam_Dao {

	@Autowired 
	@Qualifier("jdbcTemplate1")
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("entityManagerFactory1")
	private EntityManagerFactory entityManagerFactory;
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Jdbc_Payroll_Sysparam_Model getByID(String ID) {
		String sql = "select * from PAYROLL_SYSPARAM where ID = ?";
		List<Jdbc_Payroll_Sysparam_Model> data = jdbcTemplate.query(sql, new Object[] { ID }, new Jdbc_Payroll_Sysparam_Rowmapper());
		return data.get(0);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Page<Jdbc_Payroll_Sysparam_Model> ListSysparam(String name, Pageable page) {
		String count = "select count(ID) as jml from PAYROLL_SYSPARAM where ID LIKE ?";
		int total = jdbcTemplate.queryForObject(count, new Object[] { name }, (rs, rowNum) -> rs.getInt("jml"));

		String query = "Select  ID, NAME"
				+ " from ( select  ID, NAME, ROW_NUMBER() OVER (ORDER BY NAME ASC) rowRank from PAYROLL_SYSPARAM " + 
				"WHERE  NAME like ?) where rowRank BETWEEN "+page.getOffset()+" and "
				+ page.getPageSize() ;

		List<Jdbc_Payroll_Sysparam_Model> data = jdbcTemplate.query(query, new Object[] { name },
				new Jdbc_Payroll_Sysparam_Rowmapper());

		return new PageImpl<Jdbc_Payroll_Sysparam_Model>(data, page, total);
	}
	
	public Jdbc_Payroll_Sysparam_Model InsertSysparam(Jdbc_Payroll_Sysparam_Model e) {
		String query = "insert into PAYROLL_SYSPARAM(ID,NAME) VALUES(?,?)";
		jdbcTemplate.update(query,
				new Object[] {e.getID(), e.getNAME()});
		return e;
	}
	
	public Jdbc_Payroll_Sysparam_Model DetailSysparam(String id) {
		String query = "select * from PAYROLL_SYSPARAM where ID LIKE ?";
		List<Jdbc_Payroll_Sysparam_Model> data = jdbcTemplate.query(query, new Object[] { id },
				new Jdbc_Payroll_Sysparam_Rowmapper());
		return data.get(0);
	}

	public Jdbc_Payroll_Sysparam_Model UpdateSysparam(Jdbc_Payroll_Sysparam_Model e) {
		String query = "update PAYROLL_SYSPARAM set NAME=? where ID =?";
		jdbcTemplate.update(query,
				new Object[] { e.getNAME(),	e.getID() });

		return e;
	}
}
