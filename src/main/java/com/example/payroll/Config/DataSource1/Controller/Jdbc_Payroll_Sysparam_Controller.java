package com.example.payroll.Config.DataSource1.Controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.payroll.Config.DataSource1.Dao.Jdbc_Payroll_Sysparam_Dao;
import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_Sysparam_Model;
import com.example.payroll.Utils.DisplayPageUser;

@Controller
public class Jdbc_Payroll_Sysparam_Controller {
	public static Logger logger = LogManager.getLogger(Jdbc_Payroll_History_Controller.class.getName());

	@Autowired
	Jdbc_Payroll_Sysparam_Dao sysparamDao;
	
	@GetMapping(value = "/sysparam/create")
	public ModelMap add(Principal principal) {
		ModelMap data = new ModelMap();
		data.addAttribute("errorinfo", "Add Sysparam");
		data.addAttribute("user", principal.getName());
		Jdbc_Payroll_Sysparam_Model sysparamModel = new Jdbc_Payroll_Sysparam_Model();

		data.addAttribute("Jdbc_Payroll_Sysparam_Model", sysparamModel);
		return data;
	}

	@PostMapping(value = "/sysparam/create")
	public ModelAndView addProses(@Valid Jdbc_Payroll_Sysparam_Model sysparamModel, BindingResult bindingResult,
			Principal principal, HttpServletRequest request) {
		ModelMap data = new ModelMap();
		try {
			if (bindingResult.hasErrors()) {
				data.addAttribute("errorinfo", bindingResult.getAllErrors().get(0).toString());
				logger.info("error 1");
				return new ModelAndView("redirect:/sysparam/create", data);

			} else {
				sysparamDao.InsertSysparam(sysparamModel);
	        	
	        	data.addAttribute("errorinfo", "Tambah Data Sukses");
	    		return new ModelAndView("redirect:/sysparam/list", data);
			}
		} catch (Exception e) {
			data.addAttribute("errorinfo", e.toString());
			logger.error("Error Create " + e);
			return new ModelAndView("redirect:/sysparam/create", data);
		}

		// data.addAttribute("errorinfo", "Tambah Data Sukses");
		// return new ModelAndView("redirect:/VA/create", data);
	}

	@GetMapping(value = "/sysparam/list")
	public ModelMap list(Jdbc_Payroll_Sysparam_Model sysparamModel, Principal principal, Pageable page) {
		ModelMap data = new ModelMap();
		new DisplayPageUser(page, principal, data);
		if (sysparamModel.getID() == null) {
			sysparamModel.setID("");
		}
		String created = "%" + sysparamModel.getID() + "%";
		data.addAttribute("data", sysparamDao.ListSysparam(created, page));
		data.addAttribute("user", principal.getName());
		return data;
	}

	@PostMapping(value = "/sysparam/list")
	public ModelMap list2(@Valid Jdbc_Payroll_Sysparam_Model sysparamModel, Principal principal, BindingResult bindingResult,
			Pageable page) {
		return list(sysparamModel, principal, page);
	}

	@GetMapping(value = "/sysparam/detail")
	public String detail(@RequestParam String ID, Model model, Principal principal) {
		model.addAttribute("data", sysparamDao.DetailSysparam(ID));
		model.addAttribute("user", principal.getName());
		return "/sysparam/detail";
	}
	
	@GetMapping(value = "/sysparam/edit")
	public String edit(@RequestParam("ID") String id,Model model,Principal principal){
		model.addAttribute("Jdbc_Payroll_Sysparam_Model", sysparamDao.DetailSysparam(id));
		return "/sysparam/edit";
	}
	
	@PostMapping(value = "/sysparam/edit")
	public ModelAndView editProses(@Valid Jdbc_Payroll_Sysparam_Model sysparamModel,BindingResult bindingResult
			,Principal principal){
		
		ModelMap data = new ModelMap();
		if (bindingResult.hasErrors()) {
			data.addAttribute("errorinfo", bindingResult.getAllErrors().get(0));
			data.addAttribute("ID", sysparamModel.getID());
			return new ModelAndView("redirect:/sysparam/edit", data);
        }else{
        	sysparamDao.UpdateSysparam(sysparamModel);
        }
		data.addAttribute("ID", sysparamModel.getID());
		return new ModelAndView("redirect:/sysparam/detail", data);
	}
}
