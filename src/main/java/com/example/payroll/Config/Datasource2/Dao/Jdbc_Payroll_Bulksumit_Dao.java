package com.example.payroll.Config.Datasource2.Dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_Sysparam_Model;
import com.example.payroll.Config.DataSource1.Rowmapper.Jdbc_Payroll_Sysparam_Rowmapper;
import com.example.payroll.Config.Datasource2.Model.Jdbc_Payroll_Bulksumlist_Model;
import com.example.payroll.Config.Datasource2.Rowmapper.Jdbc_Payroll_Bulksumlist_Rowmapper;
import com.example.payroll.Utils.dateFormate;

@Repository
public class Jdbc_Payroll_Bulksumit_Dao {
	@Autowired 
	@Qualifier("jdbcTemplate2")
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("entityManagerFactory2")
	private EntityManagerFactory entityManagerFactory;
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Jdbc_Payroll_Bulksumlist_Model> getAll() {
		String sql = "select * from BULKSUMLIST";
		List<Jdbc_Payroll_Bulksumlist_Model> data = jdbcTemplate.query(sql, new Jdbc_Payroll_Bulksumlist_Rowmapper());
		return data;
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Page<Jdbc_Payroll_Bulksumlist_Model> Listbulksumlist(String fileName, String companyCode,String date , Pageable page) {
		String date1 = dateFormate.formatDate9(date);
//		String count = "select count(FILE_NAME) as jml from BULKSUMLIST where FILE_NAME LIKE ? and MOVE_FLAG is null";
		String count = "select count(FILE_NAME) as jml from BULKSUMLIST where"
		+" FILE_NAME like ? or CORP_ID like ? or CREATED_DT like ? "
		+ "or ( FILE_NAME like ? and CORP_ID like ? ) "
		+ "or (CORP_ID like ? and CREATED_DT like ?) "
		+ "or (FILE_NAME like ? and CREATED_DT like ?) "
		+ "or (FILE_NAME like ? and CORP_ID like ? and CREATED_DT like ?)"
		+ " AND MOVE_FLAG='N'";
		int total = jdbcTemplate.queryForObject(count, new Object[] {
				fileName, companyCode, date1,
				fileName, companyCode,
				companyCode, date1,
				fileName,  date1,
				fileName, companyCode, date1 }, (rs, rowNum) -> rs.getInt("jml"));

//		String query = "select * from BULKSUMLIST where FILE_NAME LIKE ? AND MOVE_FLAG is null AND "
//				+ "ROWNUM <=  "+ page.getPageSize();
//		String query = "select * from BULKSUMLIST where FILE_NAME LIKE ? AND MOVE_FLAG='N' AND "
//				+ "ROWNUM <=  "+ page.getPageSize();
		String query = "Select  FILE_NAME, TRANSACTION_REF_NO, CORP_ID, CORP_NAME, ACCOUNT_NO, CREATED_DT, TRX_COUNT, TOTAL_AMOUNT, ACCOUNT_BALANCE, MOVE_FLAG, IS_PROCESSED, SRVC_CD "
				+ " from ( select  FILE_NAME, TRANSACTION_REF_NO, CORP_ID, CORP_NAME, ACCOUNT_NO, CREATED_DT, TRX_COUNT, TOTAL_AMOUNT, ACCOUNT_BALANCE, MOVE_FLAG, IS_PROCESSED, SRVC_CD, ROW_NUMBER() OVER (ORDER BY FILE_NAME) rowRank from BULKSUMLIST " + 
				"WHERE  FILE_NAME like ? or CORP_ID like ? or CREATED_DT like ? "
				+ "or ( FILE_NAME like ? and CORP_ID like ? ) "
				+ "or (CORP_ID like ? and CREATED_DT like ?) "
				+ "or (FILE_NAME like ? and CREATED_DT like ?) "
				+ "or (FILE_NAME like ? and CORP_ID like ? and CREATED_DT like ?)"
				+ " AND MOVE_FLAG='N') where rowRank BETWEEN "+page.getOffset()+" and "
				+ page.getOffset()+"+20";

		List<Jdbc_Payroll_Bulksumlist_Model> data = jdbcTemplate.query(query, new Object[] { fileName, companyCode, date1,
				fileName, companyCode, 
				companyCode, date1,
				fileName,  date1,
				fileName, companyCode, date1},
				new Jdbc_Payroll_Bulksumlist_Rowmapper());

		return new PageImpl<Jdbc_Payroll_Bulksumlist_Model>(data, page, total);
	}
	
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Jdbc_Payroll_Bulksumlist_Model UpdateBulksumlist(Jdbc_Payroll_Bulksumlist_Model e) {
		String query = "update BULKSUMLIST set MOVE_FLAG='Y' where FILE_NAME =?";
		jdbcTemplate.update(query,
				new Object[] {e.getFILE_NAME() });

		return e;
	}
	
	public int[][] UpdateBulk(List<Jdbc_Payroll_Bulksumlist_Model> ez, int batchSize) {
		String query = "update BULKSUMLIST set MOVE_FLAG='Y' where FILE_NAME=?";
		int[][] updateCount = jdbcTemplate.batchUpdate(query, ez, batchSize,
				new ParameterizedPreparedStatementSetter<Jdbc_Payroll_Bulksumlist_Model>() {
					public void setValues(PreparedStatement ps, Jdbc_Payroll_Bulksumlist_Model e) throws SQLException {
						// TODO Auto-generated method stub
						ps.setString(1, e.getFILE_NAME());
					}
				});

		return updateCount;
	}
	
	public Jdbc_Payroll_Bulksumlist_Model DetailBulksumlist(String FILE_NAME) {
		String query = "select * from BULKSUMLIST where FILE_NAME LIKE ?";
		List<Jdbc_Payroll_Bulksumlist_Model> data = jdbcTemplate.query(query, new Object[] { FILE_NAME },
				new Jdbc_Payroll_Bulksumlist_Rowmapper());
		return data.get(0);
	}
	
}
