package com.example.payroll.Config.Datasource2.Model;

import java.util.Date;

import lombok.Data;

@Data
public class Jdbc_Payroll_Bulksumlist_Model {
	String FILE_NAME;
	String TRANSACTION_REF_NO;
	String CORP_ID;
	String CORP_NAME;
	String ACCOUNT_NO;
	Date CREATED_DT;
	String TRX_COUNT;
	String TOTAL_AMOUNT;
	String ACCOUNT_BALANCE;
	String MOVE_FLAG;
	String IS_PROCESSED;
	String SRVC_CD;
}
