package com.example.payroll.Config.Datasource2.Rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.payroll.Config.Datasource2.Model.Jdbc_Payroll_Bulksumlist_Model;

public class Jdbc_Payroll_Bulksumlist_Rowmapper implements RowMapper<Jdbc_Payroll_Bulksumlist_Model> {

	@Override
	public Jdbc_Payroll_Bulksumlist_Model mapRow(ResultSet rs, int rowNum) throws SQLException {
		Jdbc_Payroll_Bulksumlist_Model detail = new Jdbc_Payroll_Bulksumlist_Model();

		detail.setFILE_NAME(rs.getString("FILE_NAME"));
		detail.setTRANSACTION_REF_NO(rs.getString("TRANSACTION_REF_NO"));
		detail.setCORP_ID(rs.getString("CORP_ID"));
		detail.setCORP_NAME(rs.getString("CORP_NAME"));
		detail.setACCOUNT_NO(rs.getString("ACCOUNT_NO"));
		detail.setCREATED_DT(rs.getDate("CREATED_DT"));
		detail.setTRX_COUNT(rs.getString("TRX_COUNT"));
		detail.setTOTAL_AMOUNT(rs.getString("TOTAL_AMOUNT"));
		detail.setACCOUNT_BALANCE(rs.getString("ACCOUNT_BALANCE"));
		detail.setMOVE_FLAG(rs.getString("MOVE_FLAG"));
		detail.setIS_PROCESSED(rs.getString("IS_PROCESSED"));
		detail.setSRVC_CD(rs.getString("SRVC_CD"));
		
		return detail;
	}

}
