package com.example.payroll.Job;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

import com.example.payroll.Config.DataSource1.Dao.Jdbc_Payroll_Sysparam_Dao;
import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_Sysparam_Model;
import com.example.payroll.Tools.PayrollRestMoving;
import com.example.payroll.Utils.Constans;

@RestController
public class AutoMoveFile {

	public static Logger logger = LogManager.getLogger(AutoMoveFile.class.getName());

	@Autowired
	private Environment env;

	@Autowired
	private PayrollRestMoving payrollRestMoving;

	@Autowired
	Jdbc_Payroll_Sysparam_Dao Sysparam_Dao;

	@Scheduled(fixedRateString = "${payroll.job.status.automove.fixedrate}")
	public void scheduleTaskWithFixedRate() {
		Jdbc_Payroll_Sysparam_Model Sysparam_Model = Sysparam_Dao.getByID(Constans.JOB_ACTIVED_ID);
		if (Sysparam_Model.getNAME().equalsIgnoreCase(Constans.JOB_ACTIVED_TOOGLE)) {
			logger.info("banyak file "+payrollRestMoving.MovingFile().size());
		}

		
	
	}
}
