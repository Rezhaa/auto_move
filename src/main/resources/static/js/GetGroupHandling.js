$(document).ready(
		function() {

			// SUBMIT FORM
			$("#groupForm").submit(function(event) {
				// Prevent the form from submitting via the browser.
				event.preventDefault();
				ajaxPost();
			});

			function ajaxPost() {

				// PREPARE FORM DATA
				var formData = {
					group : $("#group").val()
				}

				// DO GET
				$.ajax({
					type : "GET",
					contentType : "application/json",
					url : "/test/user/group?group="+$("#group").val(),
					//data : JSON.stringify(formData),
					data : JSON.stringify(),
					dataType : 'json',
					success : function(result) {
						if (result.status == "Success") {
							var table = document.getElementById('tableGroup');
							var tbody = table.querySelector('tbody') || table;
							var count = tbody.getElementsByTagName('tr').length;
							var j;
							for(j=0;j<count;j++){
								var bodydata = document.getElementById('bodydata');
								bodydata.parentNode.removeChild(bodydata);
							}
							
							var i;
							for(i=0;i<result.data.length;i++){
								addRow(i,result.data[i]);
							}
						} else {
							$("#postResultDiv").html("<strong>Error</strong>");
						}
						console.log(result);
					},
					error : function(e) {
						alert("Error!")
						console.log("ERROR: ", e);
					}
				});

			}

			function createRowColumn(row) {
				  var column = document.createElement("td");
				  row.append(column);
				  return column;
			}

			function addRow(j,data) {
				  var newrow = document.createElement("tr");
				  newrow.id = "bodydata";
				  var no = createRowColumn(newrow);
				  var id = createRowColumn(newrow);
				  var group = createRowColumn(newrow);
				  var link = createLink(id,"javascript:groupHandling('"+data.id+"')");
				  
				  no.append(j+1);
				  link.append(data.id);
				  group.append(data.group_handling);

				  var table = document.getElementById('tableGroup');
				  var tbody = table.querySelector('tbody') || table;
				  
				  tbody.append(newrow);
			}
			
			function createLink(row,link) {
				  var a = document.createElement("a");
				  a.href = link;
				  row.append(a);
				  return a;
			}
		})